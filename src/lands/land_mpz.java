package lands;
import java.util.*;
import trees.tree_mpz;
import trees.tree_mzc;

public class land_mpz {
	int n;
	public String landID;
	public tree_mpz treeSet[];
	public tree_mzc treeSetMZC[];
	public land_mpz()
	{
		landID="MoPanzhong_land";
		treeSet=new tree_mpz[1];	
		treeSet[0]=new tree_mpz();
	}
	public tree_mpz[] plant(tree_mpz tree)
	{
		int i;
		tree_mpz trees[]=new tree_mpz[treeSet.length+1];
		for(i=0;i<treeSet.length;i++)
		{
			trees[i]=treeSet[i];
		}
		trees[i]=tree;
		return trees;
	}

}
