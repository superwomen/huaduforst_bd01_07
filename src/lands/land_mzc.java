package lands;
import java.util.Random;

import trees.tree_mzc;
//import java.lang.String;
public class land_mzc {
    public String landID;
    public tree_mzc treeSet[];
    public land_mzc() {
        this.landID = "mzc_land";
        treeSet = new tree_mzc[9];
    }

    public void plant(tree_mzc tree){
        int n;
        for(n = 0;n < treeSet.length;n++)
        {
            if(treeSet[n]==null)
                break;
        }
        treeSet[n]=tree;
    }


}