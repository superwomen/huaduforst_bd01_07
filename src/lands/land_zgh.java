package lands;

import trees.tree_mpz;
import trees.tree_mzc;
import trees.tree_zgh;
public class land_zgh {
	public tree_mzc treeSetMZC[];
	public tree_mpz treeSetMPZ[];
	public String landID;
	public tree_zgh treeSet[];
	public int last;
	public land_zgh() {
		landID="zgh_land";
		treeSet=new tree_zgh[50];
		last=0;
	}
	
	public void plant(tree_zgh tree){
		int i;
		treeSet[last]=new tree_zgh();
		treeSet[last].treeID=tree.treeID;
	}

}

